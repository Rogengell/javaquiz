import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static final String[] questions = {
            "Can we have two main methods in a java class?",
            "Can we have a default method without a Body ?",
            "Can we override static methods ?",
            "Is it possible to declare an Array without Array size?",
            "is this a valid declaration of a char char ch = \\utea;",
            "does the java.util package contain the Random class",
            "is main a reserved keyword in java",
            "is Import used for accessing the features of a package?",
            "Stands jar file for Java Application Resource",
            "Is this int arr[] a correct declaration of an array",
            "Is Java a object oriented program language",
            "the first bug was found in the Mark II, was it caused by a real bug?"};
    public static final boolean[] tureFalse = {
            true,
            false,
            false,
            false,
            true,
            true,
            false,
            true,
            false,
            true,
            true,
            true};
    public static Integer[] arr = new Integer[12];

    public static int rightAnswerPoint = 4;
    public static int wrongAnswerPoint = 0;
    public static int player1 = 0;
    public static int player2 = 0;
    public static int player3 = 0;
    public static int player4 = 0;

    public static void main(String[] args) {

        questionRandomized();

        int playerCount = playerNumber();
        playerControl(playerCount);;

        result(playerCount);

    }

    public static void playerControl(int playerCount){
        int count = 1;
        int roundCounter = 0;

        do {
            switch (count){
                case 1:
                    System.out.println("player 1");
                    player1 += questionSystem(roundCounter);
                    break;
                case 2:
                    System.out.println("player 2");
                    player2 += questionSystem(roundCounter);
                    break;
                case 3:
                    System.out.println("player 3");
                    player3 += questionSystem(roundCounter);
                    break;
                case 4:
                    System.out.println("player 4");
                    player4 += questionSystem(roundCounter);
                    break;
            }

            if (playerCount==count) {
                count = 0;
            }
            count++;
            roundCounter++;
        }while(roundCounter!=questions.length);

    }

    public static int playerNumber(){
        Scanner sa = new Scanner(System.in);
        System.out.println("Enter number of players from 2 to 4");
        String s = "";
        int input =0;
        boolean flag = true;

        do {

            if (sa.hasNext()) {
                s = sa.next();
                char chr = s.charAt(0);
                int ascii = chr;
                if (ascii > 48 && ascii < 57) {

                    input = Integer.parseInt(s);
                    if (input < 2 || input > 4) {
                        System.out.println("Wrong input");
                        flag = false;
                    }
                    if (input > 1 && input < 5) {
                        flag = true;
                    }
                }
                else
                {
                    System.out.println("Not a number");
                    flag = false;
                }
            }
        }while (!flag);

        return input;
    }

    public static void questionRandomized(){
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }
        Collections.shuffle(Arrays.asList(arr));
    }

    public static int questionSystem(int index) {
        Scanner sa = new Scanner(System.in);
        int pointer= arr[index];
        boolean check = false;

        System.out.println("Question: " + questions[pointer] + "\nanswer with y/n");

        int input = 0;
        do {
            input = 0;

            String answer = sa.next();

            answer = answer.toLowerCase(Locale.ROOT);
            switch (answer){
            case "y":
                check = true;
                break;
            case "n":
                check = false;
                break;
            default:
                System.out.println("Wrong Input\n");
                input = 1;
                break;
        }

        }while (input==1);

        boolean rightAnswer = tureFalse[pointer];

        if (check==rightAnswer)
        {
            System.out.println("Right answer\n");
            return rightAnswerPoint;
        }
        else
        {
            System.out.println("Wrong answer\n");
            return wrongAnswerPoint;
        }
    }

    public static void result(int playerCount){

        if (1 <= playerCount)
        {
            System.out.println("player 1: " + player1);
        }
        if (2 <= playerCount)
        {
            System.out.println("player 2: " + player2);
        }
        if (3 <= playerCount)
        {
            System.out.println("player 3: " + player3);
        }
        if (4 <= playerCount)
        {
            System.out.println("player 4: " + player4);
        }
    }
}
